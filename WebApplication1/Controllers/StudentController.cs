﻿using Comon2;
using Models;
using System;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class StudentController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Save(HttpPostedFileBase file)
        { 
            try
            {
            string imgLink = "";

                if (file.ContentLength > 0)
                {
                    string _FileName = Path.GetFileName(file.FileName);
                    imgLink = "/assets/img/" + _FileName;
                    string _path = Path.Combine(Server.MapPath("~/assets/img"), _FileName);

                    file.SaveAs(_path);
                }
           
                    
                // get info from view
                var id = Request["id"];
                var name = Request["name"];
                var dob = Request["dob"];
                var gender = Request["gender"];
                 
                var fee = Request["fee"];
                var notes = Request["notes"];
                string path = Server.MapPath("~/assets/file/StudentInfo.txt");
                string[] lines = { id, name, dob, gender, fee, notes, imgLink };
                System.IO.File.WriteAllLines(path,lines);

                return RedirectToAction("show");
            }
            catch
            {
                ViewBag.Message = "Add student failed!!!";
                return View("Show");
            }

            return View("Show");
        }

        public ActionResult show()
        {
            ViewBag.Message = "Your contact page.";

            string path = Server.MapPath("~/assets/file/StudentInfo.txt");
            string[] lines =
            System.IO.File.ReadAllLines(path);

            StudentInfo student = new StudentInfo();
            student.Id = lines[0];
            student.Name = lines[1];
            student.DOB = lines[2];
            student.Gender = lines[3];
            student.Fee = Convert.ToDouble(lines[4]);
            student.Notes = lines[5];
            student.Img = lines[6];
            System.Diagnostics.Debug.WriteLine(student.Img);

            return View("Show", student);
        }
    }
}