﻿using Comon2;
using System;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class MailController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SendMail(HttpPostedFileBase file)
        { 
            try
            {
            string _path = "";

                if (file.ContentLength > 0)
                {
                    string _FileName = Path.GetFileName(file.FileName);
                     _path = Path.Combine(Server.MapPath("~/assets/img"), _FileName);

                    file.SaveAs(_path);
                }
           
                    
            // get info from view
            var from = ConfigurationManager.AppSettings["FromEmailAddress"].ToString();
            var to = Request["to"];
            var subject = Request["subject"];
            var notes = Request["notes"];
            var content = System.IO.File.ReadAllText(Server.MapPath("~/assets/templateMail/mail.html"));

            content = content.Replace("{{From}}", from);
            content = content.Replace("{{To}}", to);
            content = content.Replace("{{Subject}}", subject);
            content = content.Replace("{{Notes}}", notes);

            System.Diagnostics.Debug.WriteLine("ok", to);

            // send mail
            new MailHelper().SendMail(to, subject, content, _path);
            ViewBag.Message = "Send mail to \"" + to + "\" success";
            }
            catch
            {
                ViewBag.Message = "Send mail failed!!!, email or pass invalid!!!";
            }
            return View("Result");
        }
    }
}